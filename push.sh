#!/bin/sh

#https://gist.github.com/praveen-palanisamy/31795f2680bc3ade04dd8d78d8651ee9
tree -I md5 -f --noreport --charset ascii | grep -vE "profiles|metadata|md5|Manifest|README|xml|repo_name|conf|directories|push|tree|LICENSE" | tail -n+2 > .tree-orig
tree -I md5 -f --noreport --charset ascii | grep -vE "profiles|metadata|md5|Manifest|README|xml|repo_name|conf|directories|push|tree|LICENSE" | sed -e 's/| \+/  /g' -e 's/[|`]-\+/ */g' -e 's:\(* \)\(\(.*/\)\([^/]\+\)\):\1[\4](\2):g' | tail -n+2 > .tree

cp -f README.tp.md README.md
printf "\n## List of all ebuilds\n" >> README.md
cat .tree >> README.md

markdown README.md > README.html

rm -f .tree .tree-orig
git add *
git commit -m "Update overlay" && git push && printf "Successfully pushed\n"
