# Copyright 2022 Gento"o Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Start an xorg server"
HOMEPAGE="https://github.com/Earnestly/sx"

LICENSE="MIT"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Earnestly/sx.git"
else
	KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
	SRC_URI="https://github.com/Earnestly/sx/archive/refs/tags/${PV}.tar.gz"
fi

IUSE=""

DEPEND="x11-base/xorg-server x11-apps/xauth"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_compile() {
	:
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr"
}
