# speedie-overlay
speedie's Gentoo GNU/Linux overlay!

# Dependencies
- Gentoo system (fairly obvious)
- app-eselect/eselect-repository or app-portage/layman
- A reason to add my overlay (optional)

## To add with eselect-repository:

`eselect repository add speedie-overlay git https://codeberg.org/speedie/speedie-overlay`

`eselect repository list`

`eselect repsitory enable <num>`

## To add with layman:

`layman -o https://codeberg.org/speedie/speedie-overlay/raw/branch/main/overlay.xml -f -a speedie-overlay`

## Usage after adding with layman:

NOTE: With layman, you need to run this command manually every time you want to sync the overlay repository.

`layman -s speedie-overlay`

## Usage adding with eselect-repository:

`emerge --sync`

The advantage of using eselect-repository is that Gentoo treats it as a regular repository and will be updated along with the official Gentoo repository. This could also be a disadvantage for various reasons, but you can probably disable this feature!

## Short link (for layman)
It may be difficult to memorize this link. You may choose to add the [https://speedie.gq/overlay.xml](https://speedie.gq/overlay.xml) link instead. This is much easier to memorize. This overlay.xml file should always lead to the latest repository anyway!

## License

The ebuilds are liensed under the GPLv2-only license due to Gentoo's licensing. I choose to respect that, despite preferring GPLv3!

