# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Minimal rautafarmi client with Vim keybinds."
HOMEPAGE="https://speedie.gq/rchat"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://raw.githubusercontent.com/speediegq/rchat/tarball/${PN}-${PV}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~~riscv ~x86"
fi

LICENSE="GPLv3"
SLOT="0"

DEPEND="
	app-shells/bash
	sys-apps/sed
	net-misc/curl
"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
