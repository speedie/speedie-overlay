# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Minimal rautafarmi client with Vim keybinds with support for the new JSON API."
HOMEPAGE="https://speedie.gq/iron"

LICENSE="GPL-3"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
	SRC_URI="https://codeberg.org/speedie/iron/releases/download/${PV}/${P}.tar.gz"
fi

IUSE=""

DEPEND="app-shells/bash sys-apps/sed net-misc/curl app-misc/jq sys-devel/make"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
