# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit toolchain-funcs

DESCRIPTION="libspeedwm: simple speedwm library"
HOMEPAGE="https://codeberg.org/speedie/libspeedwm"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi


LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="x11-libs/libX11 dev-lang/tcc"
DEPEND="${RDEPEND}"

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
