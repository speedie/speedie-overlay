# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit toolchain-funcs

DESCRIPTION="speedwm-extras: Extra scripts for speedwm providing audio, bluetooth, wireless, screenshot, screen recording, accessibility and more."
HOMEPAGE="https://codeberg.org/speedie/speedwm-extras"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi


LICENSE="GPL-3"
SLOT="0"
IUSE="+switcher +wallpaper +wifi +bluetooth +screenshot +clipboard +pulseaudio pipewire alsa"

RDEPEND="
	wallpaper? ( x11-misc/xwallpaper )
	switcher? ( x11-misc/wmctrl )
	wifi? ( net-wireless/iwd )
	screenshot? ( media-gfx/maim )
	clipboard? ( x11-misc/xclip )
	bluetooth? ( net-wireless/bluez )
	bluetooth? ( net-wireless/bluez-tools )
	pulseaudio? ( media-sound/pulsemixer )
	pipewire? ( media-sound/pulsemixer )
	alsa? ( media-sound/alsa-utils )
"
DEPEND="${RDEPEND}"

src_prepare() {
	default
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install
}
