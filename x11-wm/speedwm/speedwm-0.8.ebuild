# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit savedconfig toolchain-funcs

DESCRIPTION="speedwm: A simple fork of dwm, a window manager for X."
HOMEPAGE="https://speedie.gq/speedwm"

SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"

LICENSE="MIT"
SLOT="0"
IUSE="xinerama savedconfig +ipc +status +xresources +alttab +wallpaper +wifi +bluetooth +screenshot +clipboard +dmenu +systray +mouse +media +imlib2"

RDEPEND="
	media-libs/fontconfig
	x11-libs/libxcb
	x11-misc/xcb
	imlib2? ( media-libs/imlib2 )
	x11-libs/libX11
	x11-libs/libXft
	dev-lang/tcc
	xresources? ( x11-apps/xsetroot )
	xresources? ( x11-apps/xrdb )
	wallpaper? ( x11-misc/xwallpaper )
	alttab? ( x11-misc/wmctrl )
	status? ( x11-apps/xsetroot )
	ipc? ( dev-libs/yajl )
	wifi? ( net-wireless/iwd )
	screenshot? ( media-gfx/maim )
	clipboard? ( x11-misc/xclip )
	bluetooth? ( net-wireless/bluez )
	bluetooth? ( net-wireless/bluez-tools )
	xinerama? ( x11-libs/libXinerama )
	dmenu? ( x11-misc/dmenu-spde )
"
DEPEND="
	${RDEPEND}
	xresources? ( x11-apps/xsetroot )
	xresources? ( x11-apps/xrdb )
	wallpaper? ( x11-misc/xwallpaper )
	alttab? ( x11-misc/wmctrl )
	status? ( x11-apps/xsetroot )
	ipc? ( dev-libs/yajl )
	wifi? ( net-wireless/iwd )
	screenshot? ( media-gfx/maim )
	clipboard? ( x11-misc/xclip )
	bluetooth? ( net-wireless/bluez )
	bluetooth? ( net-wireless/bluez-tools )
	xinerama? ( x11-libs/libXinerama )
	dmenu? ( x11-misc/dmenu-spde )
"

src_prepare() {
	# status
	if ! use status; then
		PATCHES+=( "${FILESDIR}"/${P}-no-status.patch )
	fi

	default

	# disable features
	# xresources
	if ! use xresources; then
		sed -i "s/#define USEXRESOURCES    1/#define USEXRESOURCES    0/g" toggle.h
	fi

	# systray
	if ! use systray; then
		sed -i "s/#define USESYSTRAY       1/#define USESYSTRAY       0/g" toggle.h
	fi

	# mouse
	if ! use mouse; then
		sed -i "s/#define USEMOUSE         1/#define USEMOUSE         0/g" toggle.h
	fi

	# media
	if ! use media; then
		sed -i "s/#define USEMEDIA         1/#define USEMEDIA         0/g" toggle.h
	fi

	# custom layout
	#if ! use custom; then
	#	sed -i "s/#define LAYOUT_CUSTOM    1/#define LAYOUT_CUSTOM    0/g" toggle.h
	#fi

	# imlib
	if ! use imlib2; then
		sed -i "s/#define USEIMLIB2        1/#define USEIMLIB2        0/g" toggle.h
		sed -i "s/#define USEWINICON       1/#define USEWINICON       0/g" toggle.h
		sed -i "s/#define USETAGPREVIEW    1/#define USETAGPREVIEW    0/g" toggle.h
		sed -i "s/IMLIB2LIBS    = -lImlib2//g" toggle.mk
	fi

	# ipc
	if ! use ipc; then
		sed -i "s/#define USEIPC           1/#define USEIPC           0/g" toggle.h
		sed -i "s/USEIPC        = true//g" toggle.mk
		sed -i "s/YAJLLIBS      = -lyajl//g" toggle.mk
		sed -i "s|YAJLINC       = /usr/include/yajl||g" toggle.mk
	fi

	restore_config actions.h
}

src_compile() {
	if use xinerama; then
		emake CC="tcc" speedwm
	else
		emake CC="tcc" XINERAMAFLAGS="" XINERAMALIBS="" speedwm
	fi
}

src_install() {
	emake DESTDIR="${D}" CC="cc" PREFIX="${EPREFIX}/usr" install

	save_config actions.h
}
