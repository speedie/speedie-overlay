# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit savedconfig toolchain-funcs

DESCRIPTION="speedwm: A simple fork of dwm, a window manager for X."
HOMEPAGE="https://speedie.gq/speedwm"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi


LICENSE="MIT"
SLOT="0"
IUSE="+wm +xinerama +savedconfig +misc +ipc +status +xresources +switcher +wallpaper +wifi +bluetooth +screenshot +clipboard +dmenu +systray +mouse +media +imlib2 +custom"

RDEPEND="
	media-libs/fontconfig
	x11-libs/libxcb
	x11-misc/xcb
	imlib2? ( media-libs/imlib2 )
	x11-libs/libX11
	x11-libs/libXft
	dev-lang/tcc
	xresources? ( x11-apps/xrdb )
	wallpaper? ( x11-misc/xwallpaper )
	switcher? ( x11-misc/wmctrl )
	ipc? ( dev-libs/yajl )
	wifi? ( net-wireless/iwd )
	screenshot? ( media-gfx/maim )
	clipboard? ( x11-misc/xclip )
	bluetooth? ( net-wireless/bluez )
	bluetooth? ( net-wireless/bluez-tools )
	xinerama? ( x11-libs/libXinerama )
	dmenu? ( x11-misc/dmenu-spde )
"
DEPEND="${RDEPEND}"

src_prepare() {

	default

	# disable features
	# xresources
	if ! use xresources; then
		sed -i "s/#define USEXRESOURCES    1/#define USEXRESOURCES    0/g" toggle.h
		echo "" > xresources.h
	fi

	# systray
	if ! use systray; then
		sed -i "s/#define USESYSTRAY       1/#define USESYSTRAY       0/g" toggle.h
	fi

	# mouse
	if ! use mouse; then
		sed -i "s/#define USEMOUSE         1/#define USEMOUSE         0/g" toggle.h
		echo "" > mouse.h
		echo "" > statusbar.h
	fi

	# media
	if ! use media; then
		sed -i "s/#define USEMEDIA         1/#define USEMEDIA         0/g" toggle.h
	fi

	# custom layout
	if ! use custom; then
		sed -i "s/#define LAYOUT_CUSTOM    1/#define LAYOUT_CUSTOM    0/g" toggle.h
	fi

	# alt tab
	if ! use switcher; then
		sed -i "s/#define USESWITCHER      1/#define USESWITCHER      0/g" toggle.h
	fi

	# imlib
	if ! use imlib2; then
		sed -i "s/#define USEIMLIB2        1/#define USEIMLIB2        0/g" toggle.h
		sed -i "s/#define USEWINICON       1/#define USEWINICON       0/g" toggle.h
		sed -i "s/#define USETAGPREVIEW    1/#define USETAGPREVIEW    0/g" toggle.h
		sed -i "s/IMLIB2LIBS    = -lImlib2//g" toggle.mk
	fi

	# ipc
	if ! use ipc; then
		sed -i "s/#define USEIPC           1/#define USEIPC           0/g" toggle.h
		sed -i "s/USEIPC        = true//g" toggle.mk
		sed -i "s/YAJLLIBS      = -lyajl//g" toggle.mk
		sed -i "s|YAJLINC       = /usr/include/yajl||g" toggle.mk
		echo "" > ipc.h
	fi

	cat actions.h options.h text.h autostart.h colors.h rules.h xresources.h keybinds.h ipc.h mouse.h statusbar.h signal.h > config.h
	sed -i "s/actions[.]h//g" speedwm.c
	sed -i "s/options[.]h//g" speedwm.c
	sed -i "s/text[.]h//g" speedwm.c
	sed -i "s/autostart[.]h//g" speedwm.c
	sed -i "s/colors[.]h//g" speedwm.c
	sed -i "s/rules[.]h//g" speedwm.c
	sed -i "s/xresources[.]h//g" speedwm.c
	sed -i "s/keybinds[.]h//g" speedwm.c
	sed -i "s/ipc[.]h//" speedwm.c
	sed -i "s/mouse[.]h//g" speedwm.c
	sed -i "s/statusbar[.]h//g" speedwm.c
	sed -i 's/"signal[.]h/"config.h/g' speedwm.c

	# merge headers for savedconfig
	if use savedconfig; then
		if [ -e "config.h" ]; then
			restore_config config.h
		fi
	fi
}

src_compile() {
	if use xinerama; then
		emake CC="tcc" speedwm
	else
		emake CC="tcc" XINERAMAFLAGS="" XINERAMALIBS="" speedwm
	fi
}

src_install() {
	if use wm; then
		emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" install_only_bin
	else
		elog "NOTE: You have chosen to omit the wm USE flag. Without this USE flag, speedwm itself doesn't get installed, only the scripts and documentation it uses."
		elog "If you would like to install speedwm itself, set USE='wm' for x11-wm/speedwm."
	fi

	if use misc; then
		emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" install_only_misc
	else
		elog "NOTE: You have chosen to omit the misc USE flag. Without this USE flag, no documentation or scripts will be installed."
		elog "If you would like to install these, set USE='misc' for x11-wm/speedwm."
	fi

	if use savedconfig; then
		if [ -e "config.h" ]; then
			save_config config.h
		fi
	fi
}
