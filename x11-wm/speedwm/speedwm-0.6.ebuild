# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit savedconfig toolchain-funcs

DESCRIPTION="speedwm: A simple fork of dwm, a window manager for X."
HOMEPAGE="https://speedie.gq/speedwm"

SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"

LICENSE="MIT"
SLOT="0"
IUSE="xinerama savedconfig ipc status xresources"

RDEPEND="
	media-libs/fontconfig
	x11-libs/libxcb
	x11-misc/xcb
	media-libs/imlib2
	x11-libs/libX11
	x11-libs/libXft
	dev-lang/tcc
	xresources? ( x11-apps/xsetroot )
	status? ( x11-apps/xsetroot )
	ipc? ( dev-libs/yajl )
	xinerama? ( x11-libs/libXinerama )
"
DEPEND="
	${RDEPEND}
	xinerama? ( x11-base/xorg-proto )
	xresources? ( x11-apps/xsetroot )
	status? ( x11-apps/xsetroot )
	ipc? ( dev-libs/yajl )
"

src_prepare() {
	if ! use status; then
		PATCHES+=( "${FILESDIR}"/${P}-no-status.patch )
	fi

	if ! use ipc; then
		if ! use xresources; then
			PATCHES+=( "${FILESDIR}"/${P}-no-xresources-ipc.patch )
		else
			PATCHES+=( "${FILESDIR}"/${P}-no-ipc.patch )
		fi
	else
		if ! use xresources; then
			PATCHES+=( "${FILESDIR}"/${P}-no-xresources.patch )
		fi
	fi

	default
	restore_config actions.h
}

src_compile() {
	if use xinerama; then
		emake CC="tcc" speedwm
	else
		emake CC="tcc" XINERAMAFLAGS="" XINERAMALIBS="" speedwm
	fi
}

src_install() {
	emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" install

	save_config actions.h
}
