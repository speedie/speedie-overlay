# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit savedconfig toolchain-funcs

DESCRIPTION="speedwm: A simple fork of dwm, a window manager for X."
HOMEPAGE="https://speedie.gq/speedwm"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/speedwm-${PV}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="+xinerama +savedconfig +ipc +xresources +switcher +spmenu +systray +mouse +imlib2 +tcc"

PATCHES+=( "${FILESDIR}"/${PN}-1.9-norl.patch )

RDEPEND="
	media-libs/fontconfig
	x11-libs/libxcb
	x11-misc/xcb
	imlib2? ( media-libs/imlib2 )
	x11-libs/libX11
	x11-libs/libXft
	tcc? ( dev-lang/tcc )
	x11-libs/pango
	xresources? ( x11-apps/xrdb )
	ipc? ( dev-libs/yajl )
	xinerama? ( x11-libs/libXinerama )
	spmenu? ( x11-misc/spmenu )
"
DEPEND="${RDEPEND}"

src_prepare() {
	default

	# disable features
	# xresources
	if ! use xresources; then
		sed -i "s/#define USEXRESOURCES    1/#define USEXRESOURCES    0/g" toggle.h
		echo "" > xresources.h
	fi

	# systray
	if ! use systray; then
		sed -i "s/#define USESYSTRAY       1/#define USESYSTRAY       0/g" toggle.h
	fi

	# mouse
	if ! use mouse; then
		sed -i "s/#define USEMOUSE         1/#define USEMOUSE         0/g" toggle.h
		echo "" > mouse.h
		echo "" > statusbar.h
	fi

	# alt tab
	if ! use switcher; then
		sed -i "s/#define USESWITCHER      1/#define USESWITCHER      0/g" toggle.h
	fi

	# imlib
	if ! use imlib2; then
		sed -i "s/#define USEIMLIB2        1/#define USEIMLIB2        0/g" toggle.h
		sed -i "s/#define USEWINICON       1/#define USEWINICON       0/g" toggle.h
		sed -i "s/#define USETAGPREVIEW    1/#define USETAGPREVIEW    0/g" toggle.h
		sed -i "s/IMLIB2LIBS    = -lImlib2//g" toggle.mk
	fi

	# ipc
	if ! use ipc; then
		sed -i "s/#define USEIPC           1/#define USEIPC           0/g" toggle.h
		sed -i "s/YAJLLIBS      = -lyajl//g" toggle.mk
		sed -i "s|YAJLINC       = /usr/include/yajl||g" toggle.mk
		echo "" > ipc.h
	fi

	if use savedconfig; then
		if [ -e "options.h" ]; then
			restore_config options.h
		fi
	fi
}

src_compile() {
	:
}

src_install() {
	mkdir -p "${D}${EPREFIX}/usr/share/fonts"

	if use tcc; then
		if use xinerama; then
			emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" install
		else
			emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" XINERAMAFLAGS="" XINERAMALIBS="" install
		fi
	else
		if use xinerama; then
			emake DESTDIR="${D}" CC="cc" PREFIX="${EPREFIX}/usr" install
		else
			emake DESTDIR="${D}" CC="cc" PREFIX="${EPREFIX}/usr" XINERAMAFLAGS="" XINERAMALIBS="" install
		fi
	fi

	if use savedconfig; then
		if [ -e "options.h" ]; then
			save_config options.h
		fi
	fi

	elog "NOTE: speedwm no longer includes the massive list of keybinds it used to. In order to get these, install speedwm manually and get my .rl.h headers from https://codeberg.org/speedie/dotfiles and recompile, or install the speedwm-spde ebuild."
}
