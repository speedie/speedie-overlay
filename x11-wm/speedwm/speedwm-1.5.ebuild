# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit savedconfig toolchain-funcs

DESCRIPTION="speedwm: A simple fork of dwm, a window manager for X."
HOMEPAGE="https://speedie.gq/speedwm"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi


LICENSE="MIT"
SLOT="0"
IUSE="+xinerama +savedconfig savedheaders +ipc +xresources +switcher +dmenu +systray +mouse +media +imlib2 +custom"

RDEPEND="
	media-libs/fontconfig
	x11-libs/libxcb
	x11-misc/xcb
	imlib2? ( media-libs/imlib2 )
	x11-libs/libX11
	x11-libs/libXft
	dev-lang/tcc
	x11-libs/pango
	xresources? ( x11-apps/xrdb )
	ipc? ( dev-libs/yajl )
	xinerama? ( x11-libs/libXinerama )
	dmenu? ( x11-misc/dmenu-spde )
"
DEPEND="${RDEPEND}"

src_prepare() {

	default

	# disable features
	# xresources
	if ! use xresources; then
		sed -i "s/#define USEXRESOURCES    1/#define USEXRESOURCES    0/g" toggle.h
		echo "" > xresources.h
	fi

	# systray
	if ! use systray; then
		sed -i "s/#define USESYSTRAY       1/#define USESYSTRAY       0/g" toggle.h
	fi

	# mouse
	if ! use mouse; then
		sed -i "s/#define USEMOUSE         1/#define USEMOUSE         0/g" toggle.h
		echo "" > mouse.h
		echo "" > statusbar.h
	fi

	# media
	if ! use media; then
		sed -i "s/#define USEMEDIA         1/#define USEMEDIA         0/g" toggle.h
	fi

	# custom layout
	if ! use custom; then
		sed -i "s/#define LAYOUT_CUSTOM    1/#define LAYOUT_CUSTOM    0/g" toggle.h
	fi

	# alt tab
	if ! use switcher; then
		sed -i "s/#define USESWITCHER      1/#define USESWITCHER      0/g" toggle.h
	fi

	# imlib
	if ! use imlib2; then
		sed -i "s/#define USEIMLIB2        1/#define USEIMLIB2        0/g" toggle.h
		sed -i "s/#define USEWINICON       1/#define USEWINICON       0/g" toggle.h
		sed -i "s/#define USETAGPREVIEW    1/#define USETAGPREVIEW    0/g" toggle.h
		sed -i "s/IMLIB2LIBS    = -lImlib2//g" toggle.mk
	fi

	# ipc
	if ! use ipc; then
		sed -i "s/#define USEIPC           1/#define USEIPC           0/g" toggle.h
		sed -i "s/USEIPC        = true//g" toggle.mk
		sed -i "s/YAJLLIBS      = -lyajl//g" toggle.mk
		sed -i "s|YAJLINC       = /usr/include/yajl||g" toggle.mk
		echo "" > ipc.h
	fi

	if use savedheaders; then
		cat options.h text.h autostart.h colors.h rules.h xresources.h keybinds.h ipc.h mouse.h statusbar.h signal.h > config.h
		sed -i "s/options[.]h//g" speedwm.c
		sed -i "s/text[.]h//g" speedwm.c
		sed -i "s/autostart[.]h//g" speedwm.c
		sed -i "s/colors[.]h//g" speedwm.c
		sed -i "s/rules[.]h//g" speedwm.c
		sed -i "s/xresources[.]h//g" speedwm.c
		sed -i "s/keybinds[.]h//g" speedwm.c
		sed -i "s/ipc[.]h//" speedwm.c
		sed -i "s/mouse[.]h//g" speedwm.c
		sed -i "s/statusbar[.]h//g" speedwm.c
		sed -i 's/"signal[.]h/"config.h/g' speedwm.c
	fi

	# merge headers for savedheaders
	if use savedheaders; then
		if [ -e "config.h" ]; then
			restore_config config.h
		fi
	fi

	if use savedconfig; then
		if [ -e "options.h" ]; then
			restore_config options.h
		fi
	fi
}

src_compile() {
	if use xinerama; then
		emake CC="tcc" speedwm
	else
		emake CC="tcc" XINERAMAFLAGS="" XINERAMALIBS="" speedwm
	fi
}

src_install() {
	emake DESTDIR="${D}" CC="tcc" PREFIX="${EPREFIX}/usr" install

	if use savedheaders; then
		if [ -e "config.h" ]; then
			save_config config.h
		fi
	fi

	if use savedconfig; then
		if [ -e "options.h" ]; then
			save_config options.h
		fi
	fi

	elog "Notice: As of 27/10/2022, speedwm no longer includes most of the scripts that were previously bundled with it."
	elog "The scripts are not gone though. To install these scripts, install the x11-wm/speedwm-extras ebuild."
}
