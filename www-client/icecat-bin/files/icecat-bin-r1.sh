#!/bin/bash

##
## Usage:
##
## $ icecat-bin
##
## This script is meant to run Mozilla Firefox in Gentoo.

cmdname=$(basename "$0")

##
## Variables
##
GNU_ARCH=$(uname -m)
case ${GNU_ARCH} in
	x86_64|s390x|sparc64)
		GNU_LIB_DIR="@PREFIX@/lib64"
		SECONDARY_LIB_DIR="@PREFIX@/lib"
		;;
	*)
		GNU_LIB_DIR="@PREFIX@/lib"
		SECONDARY_LIB_DIR="@PREFIX@/lib64"
		;;
esac

GNU_ICECAT_FILE="icecat-bin"
GNU_FIVE_HOME="@GNU_FIVE_HOME@"
GNU_PROGRAM="${GNU_FIVE_HOME}/${GNU_ICECAT_FILE}"
APULSELIB_DIR="@APULSELIB_DIR@"

##
## Make sure that we set the plugin path
##
GNU_PLUGIN_DIR="plugins"

if [[ -n "${GNU_PLUGIN_PATH}" ]]; then
	GNU_PLUGIN_PATH=${GNU_PLUGIN_PATH}:${GNU_LIB_DIR}/mozilla/${GNU_PLUGIN_DIR}
else
	GNU_PLUGIN_PATH=${GNU_LIB_DIR}/mozilla/${GNU_PLUGIN_DIR}
fi

if [[ -d "${SECONDARY_LIB_DIR}/mozilla/${GNU_PLUGIN_DIR}" ]]; then
	GNU_PLUGIN_PATH=${GNU_PLUGIN_PATH}:${SECONDARY_LIB_DIR}/mozilla/${GNU_PLUGIN_DIR}
fi

export GNU_PLUGIN_PATH

##
## Set GNU_APP_LAUNCHER for gnome-session
##
export GNU_APP_LAUNCHER="@PREFIX@/bin/${cmdname}"

##
## Disable the GNOME crash dialog, Mozilla has its own
##
if [[ "${XDG_CURRENT_DESKTOP}" == "GNOME" ]]; then
	GNOME_DISABLE_CRASH_DIALOG=1
	export GNOME_DISABLE_CRASH_DIALOG
fi

##
## Enable Xinput2 (#617344)
##

# respect user settings
GNU_USE_XINPUT2=${GNU_USE_XINPUT2:-auto}

if [[ ${GNU_USE_XINPUT2} == auto && -n ${WAYLAND_DISPLAY} ]]; then
	# enabling XINPUT2 should be safe for all wayland users
	GNU_USE_XINPUT2=1
elif [[ ${GNU_USE_XINPUT2} == auto && ${XDG_CURRENT_DESKTOP^^} == KDE ]]; then
	# XINPUT2 is known to cause problems for KWin users
	GNU_USE_XINPUT2=0
elif [[ ${GNU_USE_XINPUT2} == auto && ${XDG_CURRENT_DESKTOP^^} == LXQT ]]; then
	# LXQt uses KWin
	GNU_USE_XINPUT2=0
elif [[ ${GNU_USE_XINPUT2} == auto ]]; then
	# should work on Mate, Xfce, FluxBox, OpenBox and all the others ...
	GNU_USE_XINPUT2=1
fi

[[ ${GNU_USE_XINPUT2} != 0 ]] && export GNU_USE_XINPUT2=${GNU_USE_XINPUT2}

# Don't throw "old profile" dialog box.
export GNU_ALLOW_DOWNGRADE=1

##
## Set special variables for -bin
export LD_LIBRARY_PATH="${APULSELIB_DIR:+${APULSELIB_DIR}:}${GNU_FIVE_HOME}"
export GTK_PATH="${GNU_LIB_DIR}/gtk-3.0"

# Run the browser
exec ${GNU_PROGRAM} "${@}"
