# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop linux-info pax-utils xdg

BASE_URI="https://ftp.gnu.org/gnu/gnuzilla/${PV}"

SRC_URI="amd64? ( ${BASE_URI}/icecat-${PV}.en-US.gnulinux-x86_64.tar.bz2 -> ${PN}_x86_64-${PV}.tar.bz2 )
	x86? ( ${BASE_URI}/icecat-${PV}.en-US.gnulinux-i686.tar.bz2 -> ${PN}_i686-${PV}.tar.bz2 )"

DESCRIPTION="GNU IceCat web browser"
HOMEPAGE="https://gnu.org/software/gnuzilla"

KEYWORDS="-* amd64 x86"
SLOT="rapid"
LICENSE="MPL-2.0 GPL-2 LGPL-2.1"
IUSE="+alsa +ffmpeg +pulseaudio selinux"

RESTRICT="strip"

BDEPEND="app-arch/unzip
	alsa? (
		!pulseaudio? (
			dev-util/patchelf
		)
	)"

COMMON_DEPEND="alsa? (
		!pulseaudio? (
			media-sound/apulse
		)
	)"

DEPEND="${COMMON_DEPEND}"

RDEPEND="${COMMON_DEPEND}
	dev-libs/atk
	dev-libs/dbus-glib
	>=dev-libs/glib-2.26:2
	media-libs/alsa-lib
	media-libs/fontconfig
	>=media-libs/freetype-2.4.10
	sys-apps/dbus
	virtual/freedesktop-icon-theme
	>=x11-libs/cairo-1.10[X]
	x11-libs/gdk-pixbuf:2
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-libs/libXcursor
	x11-libs/libXdamage
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXtst
	x11-libs/libxcb
	>=x11-libs/pango-1.22.0
	alsa? (
		!pulseaudio? ( media-sound/apulse )
	)
	ffmpeg? ( media-video/ffmpeg )
	pulseaudio? ( media-sound/pulseaudio )
	selinux? ( sec-policy/selinux-mozilla )
"

src_unpack() {
	local _src_file

	mkdir "${S}" || die

	for _src_file in ${A} ; do
			MY_SRC_FILE=${_src_file}
	done
}

src_install() {
	# Set GNU_FIVE_HOME
	local GNU_FIVE_HOME="/opt/icecat"

	dodir /opt
	pushd "${ED}"/opt &>/dev/null || die
	unpack "${MY_SRC_FILE}"
	popd &>/dev/null || die

	pax-mark m \
		"${ED}${GNU_FIVE_HOME}"/${MOZ_PN} \
		"${ED}${GNU_FIVE_HOME}"/${MOZ_PN}-bin \
		"${ED}${GNU_FIVE_HOME}"/plugin-container

	# Patch alsa support
	local apulselib=
	if use alsa && ! use pulseaudio ; then
		apulselib="${EPREFIX}/usr/$(get_libdir)/apulse"
		patchelf --set-rpath "${apulselib}" "${ED}${GNU_FIVE_HOME}/libxul.so" || die
	fi

	# Install icons
	local icon_srcdir="${ED}/${GNU_FIVE_HOME}/browser/chrome/icons/default"
	local icon_symbolic_file="${FILESDIR}/icecat-symbolic.svg"

	insinto /usr/share/icons/hicolor/symbolic/apps
	newins "${icon_symbolic_file}" ${PN}-symbolic.svg

	local icon size
	for icon in "${icon_srcdir}"/default*.png ; do
		size=${icon%.png}
		size=${size##*/default}

		if [[ ${size} -eq 48 ]] ; then
			newicon "${icon}" ${PN}.png
		fi

		newicon -s ${size} "${icon}" ${PN}.png
	done

	# Install menu
	local app_name="GNU IceCat (bin)"
	local desktop_file="${FILESDIR}/${PN}-r3.desktop"
	local desktop_filename="${PN}.desktop"
	local exec_command="${PN}"
	local icon="${PN}"

	cp "${desktop_file}" "${WORKDIR}/${PN}.desktop-template" || die

	sed -i \
		-e "s:@NAME@:${app_name}:" \
		-e "s:@EXEC@:${exec_command}:" \
		-e "s:@ICON@:${icon}:" \
		"${WORKDIR}/${PN}.desktop-template" \
		|| die

	newmenu "${WORKDIR}/${PN}.desktop-template" "${desktop_filename}"

	rm "${WORKDIR}/${PN}.desktop-template" || die

	# Install wrapper script
	[[ -f "${ED}/usr/bin/${PN}" ]] && rm "${ED}/usr/bin/${PN}"
	newbin "${FILESDIR}/${PN}-r1.sh" ${PN}

	# Update wrapper
	sed -i \
		-e "s:@PREFIX@:${EPREFIX}/usr:" \
		-e "s:@GNU_FIVE_HOME@:${GNU_FIVE_HOME}:" \
		-e "s:@APULSELIB_DIR@:${apulselib}:" \
		"${ED}/usr/bin/${PN}" \
		|| die
}
