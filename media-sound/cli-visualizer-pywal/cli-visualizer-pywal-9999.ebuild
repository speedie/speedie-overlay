# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="autogen color scheme from pywal for cli-visualizer "
HOMEPAGE="https://github.com/dpayne/cli-visualizer/"
inherit git-r3
EGIT_REPO_URI="https://github.com/speediegq/cli-visualizer_pywal"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
IUSE=""

DEPEND="app-shells/bash"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/cli-visualizer-pywal-${PV}/vis" vis
	elog "You have chosen to install cli-visualizer-pywal."
	elog "This ebuild enables Pywal support using an external script."
	elog "In order to use it, you must have Pywal installed and cli-visualizer set up."
	elog "To set up cli-visualizer, uncomment colors.scheme and set it to 'pywal' instead of 'rainbow'."
}
