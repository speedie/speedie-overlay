# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="CLI based audio visualizer "
HOMEPAGE="https://github.com/dpayne/cli-visualizer/"
inherit git-r3
EGIT_REPO_URI="https://github.com/dpayne/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
IUSE="+pywal"

DEPEND="sci-libs/fftw dev-util/cmake sys-libs/ncurses pywal? ( media-sound/cli-visualizer-pywal )"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_compile() {
	mkdir -p build
	cd build
	cmake ../
	emake clean
	path=$(pwd)
}

src_install() {
	cd $path
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr"
	if ! use pywal; then
		newbin "${WORKDIR}/${P}/build/vis" vis
	else
		newbin "${WORKDIR}/${P}/build/vis" cli-visualizer
	fi
}
