# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit desktop savedconfig toolchain-funcs

DESCRIPTION="speedie's fork of simple terminal with .Xresources, alpha, sixel and more."
HOMEPAGE="https://speedie.gq"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.speedie.gq/st"
else
	SRC_URI="https://ls.speedie.gq/releases/st-spde/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~m68k ~ppc64 ~riscv ~x86"
fi

LICENSE="MIT-with-advertising"
SLOT="0"

RDEPEND="
	>=sys-libs/ncurses-6.0:0=
	media-libs/fontconfig
	x11-libs/libX11
	x11-libs/libXft
	media-libs/harfbuzz
	media-libs/gd
	dev-lang/tcc
"

DEPEND="
	${RDEPEND}
	x11-base/xorg-proto
"
BDEPEND="virtual/pkgconfig"

src_prepare() {
	default
	restore_config options.h
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}"/usr PKG_CONFIG="$(tc-getPKG_CONFIG)" CC="tcc" install
	save_config options.h
}

pkg_postinst() {
	if ! [[ "${REPLACING_VERSIONS}" ]]; then
		elog "Please ensure a usable font is installed, like"
		elog "    media-fonts/fantasque-sans-mono"
		elog "    media-fonts/terminus-font"
		elog "    media-fonts/dejavu"
		elog "    media-fonts/urw-fonts"
		elog "                         "
		elog "For emojis to display, an emoji font must be installed, like"
		elog "    media-fonts/noto-emoji"
	fi
}
