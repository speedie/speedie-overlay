# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font xdg-utils

DESCRIPTION="Monospaced font with programming ligatures based on DejaVu Sans Mono"
HOMEPAGE="https://github.com/SSNikolaevich/DejaVuSansCode"

SRC_URI="https://github.com/SSNikolaevich/DejaVuSansCode/releases/download/v${PV}/dejavu-code-ttf-${PV}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="BitstreamVera"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~loong ~mips ppc ppc64 ~riscv ~s390 sparc x86 ~amd64-linux ~x86-linux"
IUSE="fontforge"

BDEPEND="
	fontforge? (
		app-i18n/unicode-data
		dev-perl/Font-TTF
		>=media-gfx/fontforge-20080429
		>media-libs/fontconfig-2.6.0:1.0
		>=x11-apps/mkfontscale-1.2.0
	)
"

DOCS=( AUTHORS NEWS README.md status.txt langcover.txt unicover.txt )

FONT_CONF=(
	fontconfig/20-unhint-small-dejavu-sans-code.conf
	fontconfig/57-dejavu-sans-code.conf
)
FONT_S="ttf"
FONT_SUFFIX="ttf"

src_unpack() {
	default

	if use fontforge; then
		mv "dejavu-code-ttf-${PV}" "${P}" || die
	else
		mv "dejavu-code-ttf-${PV}" "${P}" || die
	fi
}

src_prepare() {
	default
	xdg_environment_reset
}

src_compile() {
	if use fontforge; then
		emake \
			BUILDDIR=ttf \
			BLOCKS=/usr/share/unicode-data/Blocks.txt \
			UNICODEDATA=/usr/share/unicode-data/UnicodeData.txt \
			FC-LANG=/usr/share/fc-lang \
			full sans
	fi
}

src_install() {
	font_src_install
	if use fontforge; then
		dodoc ttf/*.txt
	fi
}
