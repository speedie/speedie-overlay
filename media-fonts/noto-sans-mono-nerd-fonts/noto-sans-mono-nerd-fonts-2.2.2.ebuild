# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit font check-reqs

DESCRIPTION="Nerd Fonts is a project that patches developer targeted fonts with glyphs"
HOMEPAGE="https://github.com/ryanoasis/nerd-fonts"
COMMON_URI="https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}"
TAG_URI="https://github.com/ryanoasis/nerd-fonts/raw/v${PV}"

SRC_URI="
	"${COMMON_URI}/Noto.zip"
"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="amd64 x86"

DEPEND="app-arch/unzip"
RDEPEND="media-libs/fontconfig"

CHECKREQS_DISK_BUILD="3G"
CHECKREQS_DISK_USR="4G"

IUSE="symbols symbolsmono"
REQUIRED_USE="symbols? ( !symbolsmono )"
USE="+symbols"

S="${WORKDIR}"
FONT_CONF=(
	"${S}"/10-nerd-font-symbols.conf
)
FONT_S=${S}

pkg_pretend() {
	check-reqs_pkg_setup
}

src_prepare() {
	if use symbols || use symbolsmono ; then
		install -m644 "${DISTDIR}/10-nerd-font-symbols.conf" "${S}/10-nerd-font-symbols.conf" || die
	fi

	if use symbols ; then
		install -m644 "${DISTDIR}/Symbols-2048-em_Nerd_Font_Complete.ttf" "${S}/Symbols-2048-em_Nerd_Font_Complete.ttf" || die
	fi

	if use symbolsmono ; then
		install -m644 "${DISTDIR}/Symbols-1000-em_Nerd_Font_Complete.ttf" "${S}/Symbols-1000-em_Nerd_Font_Complete.ttf" || die
	fi

	default
}

src_install() {
	declare -A font_filetypes
	local otf_file_number ttf_file_number

	otf_file_number=$(ls "${S}" | grep -i otf | wc -l)
	ttf_file_number=$(ls "${S}" | grep -i ttf | wc -l)

	if [[ ${otf_file_number} != 0 ]]; then
		font_filetypes[otf]=
	fi

	if [[ ${ttf_file_number} != 0 ]]; then
		font_filetypes[ttf]=
	fi

	FONT_SUFFIX="${!font_filetypes[@]}"

	font_src_install
}

pkg_postinst() {
	elog "Enable 50-user.conf and 10-nerd-font-symbols.conf by using"
	elog "eselect fontconfig"
}
