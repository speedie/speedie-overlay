# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit toolchain-funcs

DESCRIPTION="Simple drag-and-drop source/sink for X or Wayland"
HOMEPAGE="https://github.com/mwh/dragon"

SRC_URI="https://github.com/mwh/${PN}/archive/v${PV}.tar.gz"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="gui-libs/gtk"
DEPEND="${RDEPEND}"

src_prepare() {
	default
}

src_install() {
	emake PREFIX="${D}${EPREFIX}/usr" install
}
