# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils savedconfig toolchain-funcs

EGIT_REPO_URI="https://codeberg.org/nsxiv/nsxiv"
KEYWORDS="amd64 arm64 ppc64 ~riscv x86"

inherit git-r3

DESCRIPTION="Neo Simple X Image Viewer"
HOMEPAGE="https://nsxiv.codeberg.page"

LICENSE="GPL-2+ public-domain"
SLOT="0"
IUSE="exif gif +jpeg +png webp"

RDEPEND="
	exif? ( media-libs/libexif )
	gif? ( media-libs/giflib:0= )
	media-libs/imlib2[X,gif?,jpeg?,png?,webp?]
	x11-libs/libX11
	x11-libs/libXft
"
DEPEND="${RDEPEND}"

src_prepare() {
	restore_config config.h
	default
}

src_compile() {
	emake V=1 CC="$(tc-getCC)" HAVE_LIBEXIF=$(usex exif 1 0) HAVE_GIFLIB=$(usex gif 1 0)
}

src_install() {
	emake DESTDIR="${ED}" PREFIX=/usr install
	dodoc README.md
	domenu etc/nsxiv.desktop

	save_config config.h
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
