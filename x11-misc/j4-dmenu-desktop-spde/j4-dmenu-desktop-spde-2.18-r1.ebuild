# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

MY_PV="r${PV}"

DESCRIPTION="A fast desktop replacement for i3-dmenu-desktop. This ebuild doesn't depend on dmenu"
HOMEPAGE="https://github.com/enkore/j4-dmenu-desktop"
SRC_URI="https://github.com/enkore/j4-dmenu-desktop/archive/${MY_PV}.tar.gz -> j4-dmenu-desktop-${PV}.tar.gz"
S="${WORKDIR}/j4-dmenu-desktop-${MY_PV}"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="test"
RESTRICT="!test? ( test )"

DEPEND="test? ( dev-cpp/catch:1 )"

src_prepare() {
	cmake_src_prepare

	# Respect users CFLAGS
	sed -i -e "s/-pedantic -O2//" CMakeLists.txt || die
}

src_configure() {
	local mycmakeargs=(
		-DWITH_GIT_CATCH="no"
		-DWITH_TESTS="$(usex test)"
	)

	cmake_src_configure
}

src_install() {
	cmake_src_install

	doman j4-dmenu-desktop.1
}
