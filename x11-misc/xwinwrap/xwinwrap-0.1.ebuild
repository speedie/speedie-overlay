# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="speedie.gq's fork of xwinwrap. xwinwrap allows you to stick most of the apps to your desktop background."
HOMEPAGE="https://codeberg.org/speedie/xwinwrap"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/xwinwrap.git"
else
	SRC_URI="https://codeberg.org/speedie/xwinwrap/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="
	x11-libs/libXrender
	x11-libs/libX11
	x11-libs/libXext
"

src_prepare() {
	default
}

src_compile() {
	emake CC="cc"
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" "${PN}"
}
