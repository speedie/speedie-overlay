# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Userspace tool to disable middle mouse button paste in X11"
HOMEPAGE="https://github.com/milaq/XMousePasteBlock"
inherit git-r3
EGIT_REPO_URI="https://github.com/milaq/XMousePasteBlock.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
IUSE=""

DEPEND=""

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
