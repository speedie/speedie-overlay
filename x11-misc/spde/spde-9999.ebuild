# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Metapackage containing the spDE desktop environment."
HOMEPAGE="https://speedie.gq/projects/spde.php"

LICENSE="GPL-3"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
else
	KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
fi

USE="+xinit sx +server +pywal +shells +fonts +editor +wm +extras +terminal +runlauncher +locker +filemanager +email +temp +visualizer +pdf +music +video +flac +mixer +volumeicon +network +wifi +bluetooth +pipewire pulseaudio +compositor +image +irc +rss +torrent +browser +notification +xdg +locker +clipboard +screenshot +dev +wallpaper +git +pass +otp +htop btop +exa +conky +redshift +display +xrdb +remap"
IUSE="${USE}"

DEPEND="wm? ( x11-wm/speedwm-spde )
		extras? ( x11-wm/libspeedwm x11-wm/speedwm-extras )
		terminal? ( x11-terms/st-spde )
		runlauncher? ( x11-misc/spmenu )
		locker? ( x11-misc/slock-spde )
		runlauncher? ( x11-misc/j4-dmenu-desktop-spde )
		editor? ( app-editors/neovim )
		shells? ( app-shells/dash app-shells/zsh-resizing )
		pdf? ( app-text/zathura app-text/zathura-pdf-poppler )
		git? ( dev-vcs/git )
		email? ( mail-client/neomutt mail-client/mutt-wizard )
		fonts? ( media-fonts/fantasque-sans-mono media-fonts/noto media-fonts/noto-cjk media-fonts/noto-emoji )
		screenshot? ( media-gfx/flameshot )
		filemanager? ( media-gfx/ueberzug app-misc/vifm app-misc/vifm-ueberzug )
		image? ( media-gfx/nsxiv )
		conky? ( app-admin/conky )
		pywal? ( media-sound/cli-visualizer-pywal )
		visualizer? ( media-sound/cli-visualizer )
		music? ( media-sound/cmus app-misc/tmux )
		flac? ( media-sound/flacon )
		mixer? ( media-sound/pavucontrol )
		volumeicon? ( media-sound/volumeicon )
		media-video/ffmpeg
		pass? ( app-admin/pass )
		otp? ( app-admin/pass app-admin/pass-otp )
		filemanager? ( media-video/ffmpegthumbnailer )
		video? ( media-video/mpv )
		network? ( net-analyzer/bmon )
		irc? ( net-irc/weechat app-misc/tmux )
		rss? ( net-news/newsboat )
		torrent? ( net-p2p/qbittorrent )
		bluetooth? ( net-wireless/bluez net-wireless/bluez-tools )
		exa? ( sys-apps/exa )
		temp? ( sys-apps/lm-sensors )
		htop? ( sys-process/htop-vim )
		btop? ( sys-process/btop )
		browser? ( www-client/chromium-bin )
		x11-apps/xdpyinfo
		xrdb? ( x11-apps/xrdb )
		xinit? ( x11-apps/xinit )
		sx? ( x11-apps/sx )
		fonts? ( app-misc/fontctrl x11-apps/xfontsel )
		display? ( x11-misc/arandr x11-apps/xrandr )
		notification? ( x11-libs/libnotify x11-misc/dunst )
		compositor? ( x11-misc/picom-animations )
		locker? ( x11-misc/xautolock )
		clipboard? ( x11-misc/xclip )
		xdg? ( x11-misc/xdg-user-dirs )
		wallpaper? ( x11-misc/xwallpaper )
		wifi? ( net-wireless/iwd )
		pulseaudio? ( media-sound/pulseaudio )
		pipewire? ( media-video/pipewire )
		redshift? ( x11-misc/redshift )
		server? ( x11-base/xorg-server x11-base/xorg-drivers x11-apps/xprop )
		remap? ( x11-apps/xmodmap )
		pywal? ( app-misc/pywal )
		dev? ( dev-util/shellcheck-bin app-text/pandoc-bin app-text/ansifilter )
"

RDEPEND="${DEPEND}"

pkg_postinst() {
	if use bluetooth; then
		elog "You must enable the bluetooth service. To enable it run:"

		if use systemd; then
			elog "    systemctl enable bluetooth"
		else
			elog "    rc-update add bluetooth default"
		fi
	fi

	if use editor; then
		elog "You have USE 'editor' enabled which pulls neovim as a dependency. To use it properly, you must run ':PlugInstall' before using it."
	fi

	elog "After installation, you need to install spDE configuration files. The spDE ebuild provides a convenient shell script for this. To install spDE configuration files run:"
	elog "    spde -i"
	elog "           "
	elog "Then finally add your user to the spDE list using the following command:"
	elog "    spde -a <user>"
	elog "           "
	elog "To start spDE, run this command:"

	if use xinit; then
		elog "    startx /usr/bin/spde -r"
	else
		elog "    sx sh /usr/bin/spde -r"
	fi
}

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}

