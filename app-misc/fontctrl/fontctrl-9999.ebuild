# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Minimal font manager for GNU/Linux written in POSIX compliant shell."
HOMEPAGE="https://speedie.gq/fontctrl"

LICENSE="GPL-3+"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}"
else
	SRC_URI="https://codeberg.org/speedie/${PN}/releases/download/${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~x86"
fi

IUSE="${USE}"

DEPEND=""

src_prepare() {
	default
}

src_compile() {
	:
}

src_install() {
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
