# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Simple and lightweight GTK calendar"
HOMEPAGE="http://dmedvinsky.github.io/gsimplecal/"

LICENSE="BSD"
SLOT="0"

inherit autotools

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/dmedvinsky/${PN}.git"
	KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
fi

IUSE=""
DEPEND="sys-devel/autoconf dev-python/pkgconfig x11-libs/gtk+"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
	eautoreconf
}
