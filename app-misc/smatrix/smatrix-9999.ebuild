# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3

DESCRIPTION="A minimal, suckless replacement for cmatrix written in C."
HOMEPAGE="https://git.sr.ht/~rjraymond/smatrix"
EGIT_REPO_URI="https://git.sr.ht/~rjraymond/smatrix"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND=""

src_prepare() {
	default
}

src_install() {
	emake
	newbin "${WORKDIR}/${P}/${PN}" ${PN}
}
