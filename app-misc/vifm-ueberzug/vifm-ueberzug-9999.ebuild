# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Ueberzug support for vifm based on https://github.com/cirala/vifmimg"
HOMEPAGE="https://codeberg.org/speedie/vifm-ueberzug"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/speedie/${PN}.git"
	KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
else
	die
fi

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm ~arm64 ~ppc ~ppc64 ~riscv"
IUSE="+audio +video +pdf djvu"

DEPEND="app-shells/bash
		sys-apps/sed
		media-gfx/ueberzug
		app-misc/vifm
		audio? ( media-video/ffmpeg )
		pdf? ( app-text/poppler )
		djvu? ( app-text/djvu )
		video? ( media-video/ffmpegthumbnailer media-video/ffmpeg )"

RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	default
}

src_install() {
	newbin "${WORKDIR}/${P}/vifmrun" vifmrun
	newbin "${WORKDIR}/${P}/vifmimg" vifmimg

	elog "Note: You must add fileviewer entries to your vifmrc."
	elog "For notes on how to do this, see https://codeberg.org/speedie/vifm-ueberzug"
}
