# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module desktop

DESCRIPTION="Terminal file manager"
HOMEPAGE="https://github.com/gokcehan/${PN}"

if [[ "${PV}" = 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/gokcehan/${PN}.git"
	KEYWORDS="amd64 x86"
else
	# temporary, for some reason tarballs wont compile
	die
fi

LICENSE="MIT"
SLOT="0"
IUSE="+docs +desktop +bash zsh fish"

RDEPEND="
	dev-lang/go
	bash? ( app-shells/bash )
	zsh? ( app-shells/zsh )
	fish? ( app-shells/fish )
"

DEPEND="${RDEPEND}"

src_default() {
	default
}

src_unpack() {
	git-r3_src_unpack
	go-module_live_vendor
}

src_compile () {
	gen/build.sh || die
}

src_install() {
	dobin "${PN}"

	if use docs; then
		doman "${PN}.1"
		dodoc README.md
		docinto examples
		dodoc etc/{lf.{csh,vim},lfcd.{,c}sh,lfrc.example}
	fi

	if use bash; then
		insinto /usr/share/bash-completion/completions
		newins etc/lf.bash lf
	fi

	if use zsh; then
		insinto /usr/share/zsh/site-functions
		newins etc/lf.zsh _lf
	fi

	if use fish; then
		insinto /usr/share/fish/vendor_completions.d
		doins etc/lf.fish
		insinto /usr/share/fish/vendor_functions.d
		doins etc/lfcd.fish
	fi

	if use desktop; then
		domenu lf.desktop
	fi
}
