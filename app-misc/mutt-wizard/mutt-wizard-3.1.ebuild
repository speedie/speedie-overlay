# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A system for automatically configuring mutt and isync with a simple interface and safe passwords"
HOMEPAGE="https://muttwizard.com"

LICENSE="GPL-3+"
SLOT="0"

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/LukeSmithxyz/mutt-wizard"
else
	KEYWORDS="amd64 x86"
	SRC_URI="https://github.com/LukeSmithxyz/${PN}/archive/v${PV}.tar.gz"
fi
IUSE="+local index html autologin"

DEPEND="
    mail-client/neomutt[sasl]
	net-misc/curl
	local? ( net-mail/isync )
	autologin? ( app-crypt/gnupg )
	html? ( www-client/lynx )
	mail-mta/msmtp
	app-admin/pass
	index? ( net-mail/notmuch )"

src_prepare() {
	default
}

src_compile() {
	:
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install
}
