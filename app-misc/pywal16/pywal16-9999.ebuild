# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..10} pypy3 )
EGIT_REPO_URI="https://github.com/eylles/pywal16.git"
inherit distutils-r1 git-r3
DESCRIPTION="16 colors fork of pywal"
HOMEPAGE="https://github.com/eylles/pywal16"
KEYWORDS="~amd64 ~x86"
RESTRICT="test"

LICENSE="MIT"
SLOT="0"
IUSE=""

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND} media-gfx/imagemagick"
BDEPEND=""
